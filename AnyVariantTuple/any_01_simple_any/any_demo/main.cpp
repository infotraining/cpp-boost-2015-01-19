#include <iostream>
#include <complex>
#include <boost/any.hpp>

using namespace std;

int main()
{
    boost::any a1;

    if (a1.empty())
        cout << "a1 is empty" << endl;

    boost::any a2 = 8;
    a2 = 3.14;
    a2 = string("Tekst");

    cout << "a2 has inside: " << a2.type().name() << endl;

    a2 = complex<double>(2.0, 3.0);

    try
    {
        string text = boost::any_cast<string>(a2);

        cout << text << endl;
    }
    catch(const boost::bad_any_cast& e)
    {
        cout << "Invalid cast: " << e.what() << endl;
    }

    complex<double>* ptr_complex = boost::any_cast<complex<double> >(&a2);

    a2 = 3.14;

    if (ptr_complex)
        cout << "complex: " << *ptr_complex << endl;
    else
        cout << "Invalid cast" << endl;

}

