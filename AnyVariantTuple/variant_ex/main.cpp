#include <iostream>
#include <vector>
#include <complex>
#include <algorithm>
#include <boost/variant.hpp>
#include <boost/type_traits/is_floating_point.hpp>
#include <boost/utility/enable_if.hpp>
#include <tuple>

using namespace std;

struct AbsVisitor : public boost::static_visitor<double>
{
    template <typename T>
    typename boost::disable_if<boost::is_floating_point<T>, double>::type operator()(const T& value) const
    {
        return abs(value);
    }

    template <typename T>
    typename boost::enable_if<boost::is_floating_point<T>, double>::type
        operator()(const T& value) const
    {
        return fabs(value);
    }
};

int main()
{
    typedef boost::variant<int, float, double, complex<double> > VariantNumber;
    vector<VariantNumber> vars;
    vars.push_back(-1);
    vars.push_back(3.14F);
    vars.push_back(-7);
    vars.push_back(complex<double>(-1, -1));

    // TODO: korzystając z mechanizmu wizytacji wypisać na ekranie moduły liczb
    AbsVisitor abs_visitor;

    cout << "Modules: ";
    transform(vars.begin(), vars.end(), ostream_iterator<double>(cout, " "),
              boost::apply_visitor(abs_visitor));
    cout << endl;
}
