#include <iostream>
#include <vector>
#include <list>
#include <iterator>
#include <functional>
#include <boost/any.hpp>
#include <boost/range.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/adaptors.hpp>

using namespace std;

template <typename Type>
struct IsType
{
    bool operator()(const boost::any& value) const
    {
        return typeid(Type) == value.type();
    }
};


int main()
{
	vector<boost::any> store_anything;

	store_anything.push_back(1);
	store_anything.push_back(5);
	store_anything.push_back(string("three"));
	store_anything.push_back(3);
	store_anything.push_back(string("four"));
	store_anything.push_back(string("one"));
	store_anything.push_back(string("eight"));
	store_anything.push_back(5);
	store_anything.push_back(4);
	store_anything.push_back(boost::any());
	store_anything.push_back(string("five"));
	store_anything.push_back(string("six"));
	store_anything.push_back(boost::any());


	/* TO DO :
     * Wykorzystując algorytmy biblioteki standardowej wykonaj nastapujące czynnosci (napisz odpowiednie
	 * do tego celu predykaty lub obiekty funkcyjne):
     * 1 - przefiltruj wartosci niepuste w kolekcji stored_anything
	 * 2 - zlicz ilosc elementow typu int oraz typu string
	 * 3 - wyekstraktuj z kontenera store_anything do innego kontenera wszystkie elementy typu string
	 */

	// 1
	vector<boost::any> non_empty;

    // TODO
    boost::remove_copy_if(store_anything,
                   back_inserter(non_empty), std::mem_fn(&boost::any::empty));

	cout << "store_anything.size() = " << store_anything.size() << endl;
	cout << "non_empty.size() = " << non_empty.size() << endl;

	// 2
    int count_int = boost::count_if(non_empty, IsType<int>());
	// TODO
    cout << "non_empty przechowuje " << count_int << " elementow typu int" << endl;

    int count_string = boost::count_if(non_empty, IsType<string>());;
	// TODO
    cout << "non_empty przechowuje " << count_string << " elementow typu string" << endl;

	// 3
	list<string> string_items;
	// TODO

    string (*convert_fn)(boost::any&) = &boost::any_cast;

    boost::transform(non_empty
                        | boost::adaptors::filtered(IsType<string>())
                        | boost::adaptors::reversed,
                     back_inserter(string_items),
                     //convert_fn);
                     [](const boost::any& item) { return boost::any_cast<string>(item); });

//    for(const auto& any_item : non_empty)
//    {
//        const string* string_item = boost::any_cast<string>(&any_item);
//        if (string_item)
//            string_items.push_back(*string_item);
//    }

	cout << "string_items: ";
	copy(string_items.begin(), string_items.end(),
			ostream_iterator<string>(cout, " "));
	cout << endl;

    vector<int> v = { 1, 2, 3, 4 };

    transform(v.begin(), v.end(), v.begin(), [] (int x) { return x * x; });

    for(const auto& item : v)
    {
        cout << item << " ";
    }
    cout << endl;
}
