#include <iostream>
#include <boost/fusion/adapted.hpp>
#include <boost/fusion/algorithm.hpp>

using namespace std;

struct X
{
    int a, b;
    double d;
    char c;
};

struct Printer
{
    void operator()(int value) const
    {
        cout << "int: " << value << endl;
    }

    void operator()(double value) const
    {
        cout << "double: " << value << endl;
    }

    void operator()(char value) const
    {
        cout << "int: " << value << endl;
    }
};

BOOST_FUSION_ADAPT_STRUCT(
    X,
    (int, a)
    (int, b)
    (double, d)
    (char, c)
    )

int main()
{
    boost::fusion::vector<int, double> vec(4, 3.14);

    boost::fusion::for_each(vec, Printer());

    cout << "\n\n";

    X x = {1, 2, 4.11, 'A'};

    boost::fusion::for_each(x, Printer());

}

