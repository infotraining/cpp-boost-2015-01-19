#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <boost/tuple/tuple.hpp>
#include <boost/tuple/tuple_io.hpp>
#include <boost/tuple/tuple_comparison.hpp>

using namespace std;

boost::tuple<int, int, double> calc_stats(const vector<int>& data)
{
    vector<int>::const_iterator min_it;
    vector<int>::const_iterator max_it;
    boost::tie(min_it, max_it) = minmax_element(data.begin(), data.end());

    double avg = accumulate(data.begin(), data.end(), 0.0) / data.size();

    return boost::make_tuple(*min_it, *max_it, avg);
}

class Person
{
    string first_name_;
    string last_name_;
    int age_;
public:
    Person(const string& fn, const string& ln, int age) : first_name_(fn), last_name_(ln), age_(age)
    {}

    void print() const
    {
        cout << "Person(" << first_name_ << " " << last_name_ << ", age=" << age_ << ")" << endl;
    }

    bool operator<(const Person& p) const
    {
        return tied() < p.tied();
    }

    bool operator==(const Person& p) const
    {
        return tied() == p.tied();
    }
private:
    boost::tuple<const string&, const string&, const int&> tied() const
    {
        return boost::tie(last_name_, first_name_, age_);
    }
};

int main()
{
    vector<int> data = { 1, 6, 23, 4235, 64, -45, 345, 55, 4 };

    boost::tuple<int, int, double> stats = calc_stats(data);

    cout << "Stats: " << stats << endl;

    cout << "Min: " << stats.get<0>() << endl;
    cout << "Max: " << boost::tuples::get<1>(stats) << endl;

    int min, max;
    //double avg;

//    boost::tuple<int&, int&, double&> ref_stats(min, max, avg);
//    ref_stats = calc_stats(data);

    boost::tie(min, max, boost::tuples::ignore) = calc_stats(data);

    cout << "Min: " << min << "; Max: " << max << endl;

    vector<Person> people = { Person("Jan", "Kowalski", 33), Person("Adam", "Nowak", 76), Person("Ewa",  "Adamska", 28) };

    sort(people.begin(), people.end());

    for(const auto& p : people)
        p.print();
}

