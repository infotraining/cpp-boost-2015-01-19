#include <iostream>
#include <boost/bind.hpp>
#include <boost/function.hpp>
#include <vector>
#include <algorithm>
#include <map>

using namespace std;

void foo1(int x, int y)
{
    cout << "foo(x: " << x << ", y: " << y << ")" << endl;
}

int foo2(int value, int& counter, const std::string& text)
{
   ++counter;

    cout << text << " - " << value << endl;

    return value * value;
}

struct Add
{
    //typedef int result_type;

    int operator()(int x, int y) const
    {
        return x + y;
    }
};

class Person
{
    string first_name_;
    string last_name_;
    int age_;
public:
    Person(const string& fn, const string& ln, int age) : first_name_(fn), last_name_(ln), age_(age)
    {}

    int age() const
    {
        return age_;
    }

    void print(const string& prefix) const
    {
        cout << prefix << "(" << first_name_ << " " << last_name_ << ", age=" << age_ << ")" << endl;
    }
};

int main()
{
    auto f0 = boost::bind(&foo1, 1, 2);

    f0();  // foo1(1, 2)
    f0();  // foo1(1, 2)

    boost::function<void()> f0_alt = boost::bind(&foo1, 1, 2);

    f0_alt();
    f0_alt();

    auto f1 = boost::bind(&foo1, 7, _1);

    f1(88); // foo1(7, 88)
    f1(99); // foo1(7, 99)

    auto f2 = boost::bind(&foo1, _1, 10);

    f2(88); // foo1(88, 10)

    auto foo1 = boost::bind(&::foo1, _2, _1);

    foo1(1, 2);

    int counter = 0;

    string prefix = "Value";

    auto f3 = boost::bind(&foo2, _1, boost::ref(counter), boost::cref(prefix));

    auto result = f3(9);

    cout << "result: " << result << endl;
    cout << "counter after call: " << counter << endl;

    auto f4 = boost::bind<int>(Add(), 10, _1);

    cout << "f4() = " << f4(3) << endl;

    Person p {"Adam", "Nowak", 66 };

    auto printer = boost::bind(&Person::print, &p, "Osoba");

    printer();

    auto printer_lazy = boost::bind(&Person::print, _1, "Osoba");

    printer_lazy(p);

    vector<Person> people = { p, Person("Jan", "Kowalski", 23), Person("Ewa", "Adamska", 68) };

    for_each(people.begin(), people.end(), boost::bind(&Person::print, _1, "OSOBA"));

    map<string, int> numbers = { {"one", 1}, {"two", 2}, {"three", 3} };

    vector<string> keys;

    transform(numbers.begin(), numbers.end(), back_inserter(keys),
              boost::bind(&map<string, int>::value_type::first, _1));

    cout << "\nKeys:";

    for(const auto& k : keys)
        cout << k << " ";
    cout << endl;

    auto retired = find_if(people.begin(), people.end(),
                           boost::bind(&Person::age, _1) > 67);

    if (retired != people.end())
    {
        retired->print("Retired");
    }
}

