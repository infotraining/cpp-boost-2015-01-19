#include <iostream>
#include <string>
#include <cmath>
#include <boost/shared_ptr.hpp>
#include <boost/lexical_cast.hpp>
#include "Fraction.hpp"

using namespace std;

//template <typename T> std::string to_string(const T& arg)
//{
//	try
//	{
//		return boost::lexical_cast<std::string>(arg);
//	}
//	catch (boost::bad_lexical_cast& e)
//	{
//		return "";
//	}
//}

int main()
{
	int x = 30;
	int y = 40;
	
    string desc = std::to_string(x) + " + " + boost::lexical_cast<string>(y);
    desc = std::to_string(x) + std::to_string(y);
	
    string s1 = "30";
	string s2 = "40";
	
	int result = boost::lexical_cast<int>(s1) + boost::lexical_cast<int>(s2);
    result = stoi(s1) + stoi(s2);
	
	cout << desc << " = " << result << endl;
	
	cout << "to_string(45) = " + to_string(45) << endl;

	cout << "\n----------------------\n\n";

    // lexical_cast + klasy uzytkownika
	Fraction f1(1, 2);

	cout << "f1 = " + boost::lexical_cast<string>(f1) << endl;

	string str_fraction = "1/3";
	Fraction f2 = boost::lexical_cast<Fraction>(str_fraction);

	cout << "f1 + f2 = " << (f1 * f2) << endl;

    string hex = "FFAA";

    int hex_int = stoi(hex, 0, 16);

    cout << hex << " = " << hex_int << endl;
}

