#include <iostream>
#include <stdexcept>
#include <exception>
#define NDEBUG
#include <boost/cast.hpp>

using namespace std;

class Base
{
public:
    Base()
    {
        cout << "Base()" << endl;
    }

    virtual void run()
    {
        cout << "Base::run()" << endl;
    }

    virtual ~Base()
    {
        cout << "~Base()" << endl;
    }
};

class Derived : public Base
{
public:
    Derived()
    {
        cout << "Derived()" << endl;
    }

    virtual void run()
    {
        cout << "Derived::run()" << endl;
    }

    void derived_only()
    {
        cout << "Derived::derived_only()" << endl;
    }

    ~Derived()
    {
        cout << "~Derived()" << endl;
    }
};

int main()
{
    Base* ptr = new Derived();

    //Derived* ptr_derived = boost::polymorphic_cast<Derived*>(ptr);

    Derived* ptr_derived = boost::polymorphic_downcast<Derived*>(ptr);

    ptr_derived->derived_only();

//    Derived* ptr_derived = dynamic_cast<Derived*>(ptr);

//    if (!ptr_derived)
//        throw std::runtime_error("Bad cast");

    try
    {
        int x = 54565;

        unsigned int ux = boost::numeric_cast<unsigned int>(x);

        cout << "ux: " << ux << endl;

        char c = boost::numeric_cast<char>(x);

        cout << "c: " << static_cast<int>(c) << endl;
    }
    catch(const boost::bad_numeric_cast& e)
    {
        cout << "exception: " << e.what() << endl;
    }
}

