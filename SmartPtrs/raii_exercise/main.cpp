#include <stdio.h>
#include <stdexcept>
#include <iostream>
#include <memory>
#include <boost/noncopyable.hpp>
#include <boost/shared_ptr.hpp>

const char* get_line()
{
	static size_t count = 0;

	if (++count == 13)
		throw std::runtime_error("Blad!!!");

	return "Hello RAII\n";
}

void save_to_file(const char* file_name)
{
	FILE* file = fopen(file_name, "w");

	if ( file == 0 )
		throw std::runtime_error("Blad otwarcia pliku!!!");

	for(size_t i = 0; i < 100; ++i)
		fprintf(file, get_line());

	fclose(file);
}


class FileGuard : boost::noncopyable
{
    FILE* file_;
public:
    explicit FileGuard(FILE* file) : file_{file}
    {
        if ( file_ == 0 )
            throw std::runtime_error("Blad otwarcia pliku!!!");
    }

    ~FileGuard()
    {
        fclose(file_);
    }

    FILE* get() const
    {
        return file_;
    }
};


// TO DO: RAII
void save_to_file_with_raii(const char* file_name)
{
    FileGuard file{fopen(file_name, "w")};

    for(size_t i = 0; i < 100; ++i)
        fprintf(file.get(), get_line());
}

class X {};

void save_to_file_with_sp(const char* file_name)
{
    std::shared_ptr<void> at_scoped_exit(0, [](void* ptr) { std::cout << "ScopeExit" << std::endl;});

    FILE* file = fopen(file_name, "w");

    if (!file)
        throw std::runtime_error("Blad otwarcia pliku!!!");

    std::shared_ptr<FILE> sp_file{file, &fclose};

    for(size_t i = 0; i < 100; ++i)
        fprintf(sp_file.get(), get_line());
}

void save_to_file_with_up(const char* file_name)
{
   std::unique_ptr<FILE, int(*)(FILE*)> file(fopen(file_name, "w"), &fclose);

    if (!file)
        throw std::runtime_error("Blad otwarcia pliku!!!");

    for(size_t i = 0; i < 100; ++i)
        fprintf(file.get(), get_line());
}

int main() try
{
    boost::shared_ptr<int[]> shared_arr{new int[100]};
    shared_arr[0] = 1;


    //save_to_file("text.txt");
    //save_to_file_with_raii("text.txt");
    //save_to_file_with_sp("text.txt");
    save_to_file_with_up("text.txt");

    return 0;

}
catch(const std::exception& e)
{
    std::cout << "End of app..." << std::endl;
    std::cout << e.what() << std::endl;

    std::string temp;
    std::getline(std::cin, temp);
}
