#include <iostream>
#include <cstdlib>
#include <stdexcept>
#include <memory>
#include <vector>
#include <boost/scoped_ptr.hpp>
#include <boost/scoped_array.hpp>

using namespace std;

class Object
{
    int id_;
public:
    Object(int id = 0) : id_(id)
    {
        cout << "Object(id = " << id_ << ")" << endl;
    }

    ~Object()
    {
        cout << "~Object(id = " << id_ << ")" << endl;

    }

    void print() const
    {
        cout << "Object(id = " << id_ << ").print()" << endl;
    }
};

void may_throw()
{
    if (rand() % 2)
        throw std::runtime_error("Bad luck...");
}

void local_function()
{
    Object* ptr_obj = new Object(1);

    ptr_obj->print();

    may_throw();

    delete ptr_obj;
}

Object* object_factory(int arg)
{
    return new Object(arg);
}

void local_function_with_scoped_ptr()
{
    boost::scoped_ptr<Object> ptr_obj(object_factory(7));

    ptr_obj->print();

    may_throw();
}



void sink(Object* ptr)
{
    cout << "Inside sink..." << endl;
    ptr->print();

    delete ptr;
}

void cleanup(Object* ptr)
{
    delete ptr;
}

namespace auto_ptr_impl
{
    void local_function()
    {
        auto_ptr<Object> ptr_obj(new Object(1));

        ptr_obj->print();

        may_throw();
    }

    auto_ptr<Object> object_factory(int arg)
    {
        return auto_ptr<Object>(new Object(arg));
    }

    void sink(auto_ptr<Object> ptr)
    {
        cout << "Inside sink..." << endl;
        ptr->print();
    }
}

namespace unique_ptr_impl
{
    void local_function()
    {
        unique_ptr<Object> ptr_obj(new Object(1));

        ptr_obj->print();

        may_throw();
    }

    unique_ptr<Object> object_factory(int arg)
    {
        unique_ptr<Object> created_obj(new Object(arg));

        created_obj.reset(new Object(arg+1));

        return created_obj;
    }

    void sink(unique_ptr<Object> ptr)
    {
        cout << "Inside sink..." << endl;
        ptr->print();
    }
}

int main()
{
    try
    {
        local_function();
    }
    catch(const std::runtime_error& e)
    {
        cout << e.what() << endl;
    }

    // kopiowanie auto_ptr
    unique_ptr<Object> aptr(new Object(99));

    unique_ptr<Object> new_aptr(new Object(101));

    new_aptr = move(aptr);

    unique_ptr<Object> ptr_obj = unique_ptr_impl::object_factory(7);

    unique_ptr_impl::sink(move(ptr_obj));

    sink(new Object(9));

    unique_ptr_impl::sink(auto_ptr_impl::object_factory(10));

    vector<unique_ptr<Object>> vec;

    vec.push_back(unique_ptr<Object>(new Object(1001)));
    vec.push_back(move(new_aptr));
    vec.emplace_back(new Object(1002));


    cout << "\n\nForEach:\n";
    for(const auto& item : vec)
    {
        item->print();
    }

    auto where = vec.begin() + 1;

    auto temp = move(*where);
    vec.erase(where);

    Object* temp_ptr = temp.release();
    cleanup(temp_ptr);

    boost::scoped_array<Object> arr_objects(new Object[10]);

    arr_objects[0].print();

    std::unique_ptr<Object[]> unique_array(new Object[10]);
    unique_array[0].print();
}

