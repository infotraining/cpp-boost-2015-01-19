#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <vector>
#include <iostream>
#include <cstdlib>
#include <algorithm>

using namespace std;

class Object
{
public:
	Object(int val = 0) : val_(val) 
	{
		cout << "Konstrukcja obiektu Object " << val_ << "\n";
	}
	
	~Object()
	{
		cout << "Destrukcja obiektu Object " << val_ << "\n";
	}
	
	void print() const
	{
		cout << "Działa obiekt klasy Object " << val_ << "\n";
	}
private:
	int val_;
};

int main()
{
	boost::shared_ptr<Object> external;
	try
	{
		std::vector< boost::shared_ptr<Object> > vec;
		
		vec.push_back(boost::shared_ptr<Object>(new Object(1)));
        vec.push_back(boost::make_shared<Object>(2));
        vec.push_back(boost::make_shared<Object>(3));
        vec.emplace_back(new Object(4));
		
		vec[0]->print();

		external = vec[0];
		
		throw int(5);
	}
	catch(...)
	{
		cout << "Zlapalem wyjątek\n";
	}

    cout << "End of app..." << endl;
}
