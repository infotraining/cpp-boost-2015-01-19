#include <memory>
#include <iostream>
#include <exception>
#include <stdexcept>
#include <boost/scoped_ptr.hpp>
#include <boost/scoped_array.hpp>

using namespace std;

class Gadget
{
public:
	// konstruktor
    Gadget(int value = 0)
        : value_ {value}
	{
        std::cout << "Konstruktor Gadget(" << value_ << ")\n";
	}
	
	// destruktor
    ~Gadget()
	{
        std::cout << "Destruktor ~Gadget(" << value_ << ")\n";
	}
	
    int value() const
	{
		return value_;
	}

    void set_value(int value)
	{
        value_ = value;
	}

    void unsafe()
    {
        throw std::runtime_error("ERROR");
    }

private:
	int value_;
};

void reset_value(Gadget& g, int n)
{
    // some logic

    g.set_value(n * n);
    cout << "New value for Gadget: " << g.value() << endl;
}

std::unique_ptr<Gadget> factory(int arg) // TODO: poprawa z wykorzystaniem smart_ptr
{
    return std::unique_ptr<Gadget>(new Gadget(arg));
}

Gadget* legacy_array_factory(unsigned int size)
{
    Gadget* xarray = new Gadget[size];

    for(unsigned int i = 0; i < size; ++i)
        xarray[i].set_value(i);

    return xarray;
}

void unsafe1()  // TODO: poprawa z wykorzystaniem smart_ptr
{
    std::unique_ptr<Gadget> ptr_gdgt = factory(4);

	/* kod korzystajacy z ptrX */

    reset_value(*ptr_gdgt, 5);

    ptr_gdgt->unsafe();
}

void unsafe2()
{
    int size = 10;

    std::unique_ptr<Gadget[]> buffer(legacy_array_factory(size));

    /* kod korzystający z buffer */

    for(int i = 0; i < size; ++i)
        buffer[0].unsafe();
}

int main() try
{
    try
    {
        unsafe1();
    }
    catch(const exception& e)
    {
        cout << "Exception caught: " << e.what() << endl;
    }

    unsafe2();
}
catch(const exception& e)
{
    cout << "Exception caught: " << e.what() << endl;
}
