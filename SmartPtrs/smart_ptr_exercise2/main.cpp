#include <iostream>
#include <cstdlib>
#include <exception>
#include <stdexcept>
#include <memory>
#include <cassert>
#include <vector>

using namespace std;

/********************************************************************
*  Uodparnianie konstrukora na wyjątki #1
********************************************************************/

class Device
{
private:
	size_t devno_;
public:
	Device(int devno) : devno_(devno)
	{
		if (devno == 2)
			throw std::runtime_error("Powazny problem!");

		cout << "Konstruktor Device #" << devno << endl;
	}

	~Device()
	{
		cout << "Destruktor Device #" << devno_ << endl;
	}

};


class Broker 
{
public:
    Broker(int devno1, int devno2) : dev1_(new Device(devno1)), dev2_(new Device(devno2))
	{
	}

    Broker(Broker&& source) = default;
    Broker& operator=(Broker&& source) = default;

    ~Broker() = default;

    Device* dev1() const
    {
        return dev1_.get();
    }

    Device* dev2() const
    {
        return dev2_.get();
    }

private:
    std::unique_ptr<Device> dev1_;
    std::unique_ptr<Device> dev2_;
};

class X
{
    std::vector<std::string> words_;
public:
    X() = default;
    X(const X& source) = default;
    X& operator=(const X&) = default;
    X(X&& source) = default;
};

int main()
{

    X x;

    X cx = x;

    cx = x;

    cx = move(cx);

	try
	{
        Broker b(1, 3);
        Broker copy_b = move(b);

        assert(b.dev1() == nullptr);
        assert(b.dev2() == nullptr);
	}
	catch(const exception& e)
	{
		cerr << "Wyjatek: " << e.what() << endl;
	}
}
