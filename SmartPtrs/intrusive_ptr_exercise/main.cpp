#include <iostream>
#include <boost/intrusive_ptr.hpp>
#include <cstdlib>

class IUnknown
{
public:
    IUnknown() : ref_count_(0)
    {
    }

    void AddRef()
    {
        ++ref_count_;
    }

    int Release()
    {
        --ref_count_;

        if (ref_count_ == 0)
            delete this;
    }

    virtual ~IUnknown() {}
private:
    int ref_count_;
};

class COMObject : public IUnknown
{
public:
    COMObject(int id) :id_(id)
    {
        std::cout << "Ctor COM object - id: " << id_ << std::endl;
    }

    ~COMObject()
    {
        std::cout << "Destructor of COM object - id: " << id_ << std::endl;
    }
private:
    int id_;
};

// TODO - zdefiniować funkcje pomocnicze dla wskaznika intrusive_ptr przechowujacego COMObject
void intrusive_ptr_add_ref(IUnknown* obj)
{
    obj->AddRef();
}

void intrusive_ptr_release(IUnknown* obj)
{
    obj->Release();
}

int main()
{
    boost::intrusive_ptr<COMObject> outer_ptr;

	std::cout << "Przed wejsciem do zasiegu" << std::endl;
	{
        boost::intrusive_ptr<COMObject> p1(new COMObject(1));
		{
            boost::intrusive_ptr<COMObject> p2 = p1;
            outer_ptr = p2;
		}
	}

	std::cout << "Po wyjsciu z zasiegu" << std::endl;
}
