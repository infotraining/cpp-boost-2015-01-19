#include <iostream>
#include <memory>

using namespace std;

class Base
{
public:
    Base()
    {
        cout << "Base()" << endl;
    }

    virtual void run()
    {
        cout << "Base::run()" << endl;
    }

    virtual ~Base()
    {
        cout << "~Base()" << endl;
    }
};

class Derived : public Base
{
public:
    Derived()
    {
        cout << "Derived()" << endl;
    }

    virtual void run()
    {
        cout << "Derived::run()" << endl;
    }

    void derived_only()
    {
        cout << "Derived::derived_only()" << endl;
    }

    ~Derived()
    {
        cout << "~Derived()" << endl;
    }
};

int main()
{
    shared_ptr<Base> ptr_base(new Base());

    ptr_base->run();

    shared_ptr<Derived> ptr_derived = dynamic_pointer_cast<Derived>(ptr_base);

    if (ptr_derived)
        ptr_derived->derived_only();


}

