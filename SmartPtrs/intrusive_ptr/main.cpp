#include <iostream>
#include <boost/intrusive_ptr.hpp>
#include <cstdlib>

// definicja funkcji intrusive_ptr_add_ref i intrusive_ptr_release
template <typename T>
void intrusive_ptr_add_ref(T* t)
{
	t->add_ref();
}

template <typename T>
void intrusive_ptr_release(T* t)
{
	if (t->release() <= 0)
		delete t;
}

// klasa licznika
class reference_counter
{
	int ref_count_;
public:
	reference_counter() : ref_count_(0) {}
	
	virtual ~reference_counter() {}
	
	void add_ref()
	{
		++ref_count_;
	}
	
	int release()
	{
		return --ref_count_;
	}

private:
	// blokada dostepu do konstruktora kopiujacego
	reference_counter(const reference_counter&);
};

// zarzadzana klasa
class some_class : public reference_counter
{
public:
	some_class()
	{
		std::cout << "some_class::some_class()\n";
	}
	
	some_class(const some_class& other)
	{
		std::cout <<"some_class::some_class(const some_class& other)\n";
	}
	
	~some_class()
	{
		std::cout << "some_class:~some_class()\n";
	}
};

int main()
{
	std::cout << "Przed wejsciem do zasiegu" << std::endl;
	{
		some_class* p = new some_class();
		boost::intrusive_ptr<some_class> p1(p);
		{
			boost::intrusive_ptr<some_class> p2(p);
		}
	}
	std::cout << "Po wyjsciu z zasiegu" << std::endl;
}
