#include "bitmap.hpp"
#include <algorithm>
#include <iostream>
#include <vector>
#include <deque>
#include <boost/ref.hpp>

struct Bitmap::Impl
{
    std::deque<char> buffer_;

    Impl(size_t size, char c) : buffer_(size, c)
    {
            std::cout << "Impl()" << std::endl;
    }

    ~Impl()
    {
        std::cout << "~Impl()" << std::endl;
    }
};

Bitmap::Bitmap(size_t size)
{
    impl_.reset(new Impl(size, 65));
}

Bitmap::~Bitmap() = default;

void Bitmap::draw() const
{
    for(int i = 0; i < impl_->buffer_.size(); ++i)
        std::cout << impl_->buffer_[i];
    std::cout << std::endl;
}
