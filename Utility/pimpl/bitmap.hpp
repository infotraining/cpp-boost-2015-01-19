#ifndef BITMAP_HPP
#define BITMAP_HPP

#include <vector>
#include <memory>
#include <iostream>
#include <boost/scoped_ptr.hpp>

class Bitmap
{     
    class Impl;

    std::unique_ptr<Impl> impl_;
public:
    Bitmap(std::size_t size);
    ~Bitmap();

    void draw() const;    
};

#endif // BITMAP_HPP


