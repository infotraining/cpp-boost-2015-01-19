#include <iostream>
#include <boost/utility/enable_if.hpp>
#include <boost/type_traits/is_arithmetic.hpp>

using namespace std;

template <typename T>
typename boost::enable_if<boost::is_integral<T>, T>::type foo(const T& value)
{
    cout << "foo<integral>(" << value << ")" << endl;

    return value;
}

template <typename T>
typename boost::disable_if<boost::is_integral<T>, T>::type foo(const T& value)
{
    cout << "foo<T>(" << value << ")" << endl;

    return value;
}

struct Int
{
    int value;
};

ostream& operator<<(ostream& out, const Int& item)
{
    out << item.value;

    return out;
}

int main()
{
    int x = 10;
    foo(x);

    short sx = 20;
    foo(sx);
    foo(3.14);

    Int my_int{10};
    auto value = foo(my_int);

    cout << "Value: " << value << endl;
}

