#include <iostream>
#include <vector>
#include <memory>
#include <boost/static_assert.hpp>
#include <boost/type_traits/is_base_of.hpp>
#include <boost/type_traits/is_integral.hpp>

class Base {};

class Derived : public Base {};

class Different {};

template <typename T>
class OnlyCompatibleWithIntegralTypes
{
    //BOOST_STATIC_ASSERT(boost::is_integral<T>::value);

    static_assert(boost::is_integral<T>::value, "T musi byc typem calkowitym");
};

template <typename T, size_t i>
void accepts_arrays_with_size_between_1_and_100(T (&arr)[i])
{
	BOOST_STATIC_ASSERT(i>=1 && i<=100);
}

void expects_ints_to_be_4_bytes()
{
	BOOST_STATIC_ASSERT(sizeof(int)==4);
}

template <typename T>
void works_with_base_and_derived(T& t)
{
	BOOST_STATIC_ASSERT((boost::is_base_of<Base, T>::value));
}


template <typename Pointer>
struct IsAutoPtr : public boost::false_type
{};

template <typename T>
struct IsAutoPtr<std::auto_ptr<T>> : public boost::true_type
{};

template <typename Pointer>
void use_pointer(Pointer ptr)
{
    static_assert(!IsAutoPtr<Pointer>::value, "Pointer can't be std::auto_ptr");

    std::cout << "Value: " << *ptr << std::endl;
}

int main()
{
    BOOST_STATIC_ASSERT(boost::is_integral<bool>::value);

    OnlyCompatibleWithIntegralTypes<int> test1;

    int arr[50];

	accepts_arrays_with_size_between_1_and_100(arr);

	Derived arg;
    works_with_base_and_derived(arg);

    int x = 10;
    use_pointer(&x);

    std::shared_ptr<int> sp = std::make_shared<int>(10);
    use_pointer(sp);

    std::auto_ptr<int> ap(new int(20));
    //use_pointer(ap);

    std::cout << *ap << std::endl;
}
