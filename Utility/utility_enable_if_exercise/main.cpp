#include <iostream>
#include <iterator>
#include <list>
#include <boost/type_traits/is_pod.hpp>
#include <boost/utility/enable_if.hpp>
#include <cstring>
#include <typeinfo>

using namespace std;

// 1 - napisz generyczny algorytm mcopy kopiujący zakres elementow typu T [first, last) do kontenera rozpoczynającego się od dest
template <typename InIter, typename OutIter>
void mcopy(InIter start, InIter end, OutIter dest)
{
    cout << "Default version for types: " << typeid(InIter).name()
         << " and " << typeid(OutIter).name() << endl;
    while (start != end)
    {
        *dest = *start;

        ++dest;
        ++start;
    }
}

// 2 - napisz zoptymalizowaną wersję mcopy wykorzystującą memcpy dla tablic T[] gdzie typ T jest typem POD
template <typename T>
typename boost::enable_if<boost::is_pod<T> >::type mcopy(T* start, T* end, T* dest)
{
    cout << "Optimized version for type: " << typeid(T).name() << endl;
    memcpy(dest, start, (end - start) * sizeof(T));
}

template <typename T1, typename T2>
struct AreTheSameSize
{
    static constexpr bool value = (sizeof(T1) == sizeof(T2));
};

template <typename T1, typename T2>
typename boost::enable_if<AreTheSameSize<T1, T2>>::type mcopy(T1* start, T1* end, T2* dest)
{

}


int main()
{
    string words[] = { "one", "two", "three", "four" };

    list<string> list_of_words(4);

    mcopy(words, words + 4, list_of_words.begin()); // działa wersja generyczna

    mcopy(list_of_words.begin(), list_of_words.end(), ostream_iterator<string>(cout, " "));
    cout << "\n";

    int numbers[] = { 1, 2, 3, 4, 5 };
    int target[5];

    mcopy(numbers, numbers + 5, target); // działa wersja zoptymalizowana

    mcopy(target, target + 5, ostream_iterator<int>(cout, " "));
    cout << "\n";

    string tab1[] = { "Ala", "ma", "kota" };
    string tab2[3];

    mcopy(tab1, tab1+3, tab2);
}

