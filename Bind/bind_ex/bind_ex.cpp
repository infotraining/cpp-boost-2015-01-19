#include "person.hpp"
#include "utils.hpp"

#include <iostream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <functional>
#include <boost/bind.hpp>

using namespace std;

int main()
{
    vector<Person> employees;
    fill_person_container(employees);

    cout << "Wszyscy pracownicy:\n";
    copy(employees.begin(), employees.end(), ostream_iterator<Person>(cout, "\n"));
    cout << endl;

    // wyświetl pracownikow z pensją powyżej 3000
    cout << "\nPracownicy z pensja powyżej 3000:\n";
    copy_if(employees.begin(), employees.end(),
            ostream_iterator<Person>(cout, "\n"),
            boost::bind(&Person::salary, _1) > 3000.0);


    // wyświetl pracowników o wieku poniżej 30 lat
    cout << "\nPracownicy o wieku poniżej 30 lat:\n";
    copy_if(employees.begin(), employees.end(),
            ostream_iterator<Person>(cout, "\n"),
            boost::bind(&Person::age, _1) > 30 && boost::bind(&Person::age, _1) < 40 );

    // posortuj malejąco pracownikow wg nazwiska
    cout << "\nLista pracowników wg nazwiska (malejaco):\n";
    sort(employees.begin(), employees.end(),
         boost::bind(&Person::name, _1) > boost::bind(&Person::name, _2));

    // wyświetl kobiety
    cout << "\nKobiety:\n";
    copy_if(employees.begin(), employees.end(),
            ostream_iterator<Person>(cout, "\n"),
            boost::bind(&Person::gender, _1) == Female);

    // ilość osob zarabiajacych powyżej średniej    
    // cout << "\nIlosc osob zarabiajacych powyzej sredniej:\n";
}
