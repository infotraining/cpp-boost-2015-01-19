#include <iostream>
#include <queue>
#include <boost/function.hpp>
#include <boost/bind.hpp>
#include <memory>

using namespace std;

class Worker
{
    typedef boost::function<void ()> CommandType;
    queue<CommandType> tasks_;
public:
    void register_task(CommandType cmd)
    {
        tasks_.push(cmd);
    }

    void run()
    {
        while(!tasks_.empty())
        {
            CommandType cmd = tasks_.front();
            cmd();
            tasks_.pop();
        }
    }
};

class Printer
{
public:
    ~Printer() { cout << "~Printer()" << endl; }

    void print(const string& text)
    {
        cout << "Print:" << text << endl;
    }

    void off()
    {
        cout << "Printer.off\n";
    }

    void on()
    {
        cout << "Printer.on\n";
    }
};

void mylog(const string& message)
{
    cout << "Log: " << message << endl;
}

Worker worker;

int main()
{
    {
        std::shared_ptr<Printer> prn = std::make_shared<Printer>();

        worker.register_task(boost::bind(&Printer::on, prn));
        worker.register_task(boost::bind(&Printer::print, prn, "Tekst"));
        worker.register_task(boost::bind(&Printer::off, prn));
        worker.register_task(boost::bind(&mylog, "End of work..."));
        worker.register_task([]{ mylog("End of work..."); });

        //...
    }

    worker.run();

    return 0;
}

